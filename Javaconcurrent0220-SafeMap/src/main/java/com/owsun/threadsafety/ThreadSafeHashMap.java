package com.owsun.threadsafety;

import java.util.HashMap;


public class ThreadSafeHashMap<K,V> {


    private final HashMap<K, V> hashMap;

    public ThreadSafeHashMap() {
        hashMap = new HashMap<>();
    }



    public V put(K key, V value) {
        synchronized(hashMap) {
            return hashMap.put(key, value);
        }
    }

    public V get(Object key) {
        return hashMap.get(key);
    }

    public int size() {
        return hashMap.size();
    }
}
