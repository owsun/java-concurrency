package com.owsun.threadsafety;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VerifyMain {


    private static void testMethod1() throws InterruptedException {
        Map hashMap = new HashMap();
        ExecutorService esHashMap = Executors.newCachedThreadPool();
        CountDownLatch cdlHashMap = new CountDownLatch(1000);
        for(int i = 0; i< 1000; i++) {
            esHashMap.execute(() -> {
                hashMap.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
                cdlHashMap.countDown();
            });
        }
        cdlHashMap.await();
        System.out.println(hashMap.getClass().getSimpleName() + " is thread " + (hashMap.size() == 1000 ? "safe" : "unsafe"));
        esHashMap.shutdownNow();
    }


    private static void testMethod2() throws InterruptedException {
        ThreadSafeHashMap threadSafeHashMap = new ThreadSafeHashMap();
        ExecutorService esThreadSafeHashMap = Executors.newCachedThreadPool();
        CountDownLatch cdlThreadSafeHashMap = new CountDownLatch(1000);
        for(int i = 0; i< 1000; i++) {
            esThreadSafeHashMap.execute(() -> {
                threadSafeHashMap.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
                cdlThreadSafeHashMap.countDown();
            });
        }
        cdlThreadSafeHashMap.await();
        System.out.println(threadSafeHashMap.getClass().getSimpleName() + " is thread " + (threadSafeHashMap.size() == 1000 ? "safe" : "unsafe"));
        esThreadSafeHashMap.shutdownNow();
    }

    public static void main(String[] args) throws InterruptedException {


//        testMethod1();
//
//        testMethod2();

        String headerRow = "1,2,,,,,,,,,";

        String dataRow = "1,2,,,,,,,,,";


        int count = headerRow.split(",", -1).length;
        System.out.println(count);

//        System.out.println(count);
//
//        String test = getDefinitionRowWithCommaString(count,"11,1");//StringUtils.rightPad("123456", count,",");
//
//        System.out.println(test);
//
//        count = test.split(",", -1).length;
//
//        System.out.println(count);
//
//
//        headerRow += "{0}";
//        Object[] tmp = {"\r\n"};
//
//        System.out.println(doSubstitutions(headerRow, tmp));
//        System.out.println(doSubstitutions(headerRow, tmp));
    }


    public static String getDefinitionRowWithCommaString(int headerRowCommaCount, String definitionRow){
        if(definitionRow == null){
            return null;
        }
        int definitionRowCommaCount = definitionRow.split(",").length;
        if(definitionRowCommaCount >= headerRowCommaCount){
            return definitionRow;
        }

        int commaDifferCount = headerRowCommaCount - definitionRowCommaCount;
        StringBuilder DefinitionCommaRowBuilder = new StringBuilder();
        DefinitionCommaRowBuilder.append(definitionRow);
        for(int i = 0; i< commaDifferCount; i++){
            DefinitionCommaRowBuilder.append(",");
        }

        return DefinitionCommaRowBuilder.toString();
    }

    public static String doSubstitutions(String template, Object[] substitutions, int... encodingOptions) {
        // this is what we are going to do, we will try to do it with the message format class,
        // but if tat fails we will do it ourselves
        String toReturn = null;

        // Forced use of custom format routine because of MessageFormat.format
        // problems with single-quotes (it threats them like comments and do not process)
		/*try{
			toReturn = java.text.MessageFormat.format(template, substitutions);
		}
		catch (Exception e)*/
        {

            String[] startEndTags = new String[]{"{", "}"};
            String START = startEndTags[0];
            String END = startEndTags[1];

            StringBuffer sb = new StringBuffer();
            sb.ensureCapacity(template.length());

            // char curr = 0;
            int digitIndex = -1;
            int endIndex = -1;
            String replacement = null;
            for (int i = 0; i < template.length(); i++) {
                replacement = null;
                // curr = template.charAt(i);

                // is the current char a starting bracket
                if (template.substring(i).startsWith(START)) {
                    digitIndex = i + START.length();
                    endIndex = i + START.length() + 1;

                    // if the current digit and current end are less than the length,
                    // we may be able to replace
                    if ((digitIndex < template.length()) && (endIndex < template.length())) {

                        // if a digit follows the starting bracket, we may still be able to replace
                        if (Character.isDigit(template.charAt(digitIndex))) {

                            // the ending bracket follows the starting bracket, we can still replace
                            if (template.substring(endIndex).startsWith(END)) {
                                replacement = getSubstitution(template.charAt(digitIndex), substitutions);
                                // now set i == to the end index if we fond a replacement
                                if (replacement != null) {
                                    i = (endIndex + END.length()) - 1;
                                }
                            } // end of end bracket is in right place
                        } // end of if the character is a replacement
                    } // end of if the current digit and current end are less than total length
                } // end of if the current char is a starting bracket

                if (replacement != null) {
                    sb.append(replacement);
                } else {
                    sb.append(template.charAt(i));
                } // end of else
            } // end of for
            toReturn = sb.toString();
        }
        return toReturn;
    }

    private static String getSubstitution(char theChar, Object[] localSubstitutions) {
        String replacement = "";

        if (Character.isDigit(theChar)) {
            int substitutionIndex = theChar - '0';
            if (substitutionIndex > (localSubstitutions.length - 1)) {
                return null;
            }
            replacement = localSubstitutions[substitutionIndex].toString();
        }

        return replacement;
    }


}
