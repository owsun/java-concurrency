package com.owsun.threadsafety;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadSafetyVerification {

    public static void main(String[] args) throws InterruptedException {
        // Use the synchronized
        SafeSequence safeSequence = new SafeSequence();
        ExecutorService es = Executors.newCachedThreadPool();
        CountDownLatch cdl = new CountDownLatch(1000);
        for(int i = 0; i< 1000; i++) {
            es.execute(() -> {
                safeSequence.upCount();
                cdl.countDown();
            });
        }
        cdl.await();
        System.out.println(safeSequence.getValue() == 1000);  // Result is : 1000

        // Use the AtomicLong
        SafeAtomicLongSequence safeAtomicLongSequence = new SafeAtomicLongSequence();

        ExecutorService esAtomicLong = Executors.newCachedThreadPool();
        CountDownLatch cdlAtomicLong = new CountDownLatch(1000);
        for(int i = 0; i< 1000; i++) {
            esAtomicLong.execute(() -> {
                safeAtomicLongSequence.upCount();
                cdlAtomicLong.countDown();
            });
        }
        cdlAtomicLong.await();
        System.out.println(safeAtomicLongSequence.getValue() == 1000); // Result is : 1000


        // Use the same way to verify it, the result always is true.


    }
}
