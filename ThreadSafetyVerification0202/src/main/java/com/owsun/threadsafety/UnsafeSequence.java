package com.owsun.threadsafety;

public class UnsafeSequence {
    private int count;


    public int upCount() {
        return count++;
    }

    public int getValue() {
        return count;
    }


}
