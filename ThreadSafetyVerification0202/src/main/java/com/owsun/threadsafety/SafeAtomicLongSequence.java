package com.owsun.threadsafety;

import java.util.concurrent.atomic.AtomicLong;

public class SafeAtomicLongSequence {
    private AtomicLong count = new AtomicLong(0);;


    public void upCount() {
        count.incrementAndGet();
    }

    public long getValue() {
        return count.get();
    }


}
