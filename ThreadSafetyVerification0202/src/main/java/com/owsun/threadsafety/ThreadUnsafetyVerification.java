package com.owsun.threadsafety;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadUnsafetyVerification {

    public static void main(String[] args) throws InterruptedException {


//        ExecutorService es = Executors.newCachedThreadPool();
//        int threadCount = 1000;
//        CountDownLatch cdl = new CountDownLatch(threadCount);
//        for(int i = 0; i< threadCount; i++) {
//           es.execute(() -> {
//               unsafeSequence.upCount();
//               cdl.countDown();
//           });
//        }
//        cdl.await();
//        // 使用1000 个线程访问变量 做累加操作 每个线程累加一次, 如果是线程安全的  最后的结果应该是1000
//        // 但是现在结果小于1000, 证明一些线程访问了访问到了相同的值.
//        System.out.println(unsafeSequence.getValue() == threadCount); // Result is : 943
//        es.shutdownNow();
        boolean result = false;
        do {

            UnsafeSequence unsafeSequence = new UnsafeSequence();

            ConcurrentLinkedQueue queue = new ConcurrentLinkedQueue<>();
            ConcurrentLinkedQueue queue1 = new ConcurrentLinkedQueue<>();

            new Thread(() -> {
                for (int i =0;i< 1000; i++ ) {
                    queue.add(unsafeSequence.upCount());
                }
            }).start();

            new Thread(() -> {
                for (int i =0;i< 1000; i++ ) {
                    queue1.add(unsafeSequence.upCount());
                }
            }).start();



            result = queue.stream().anyMatch(item -> queue1.stream().anyMatch(item2 -> item2 == item));


            System.out.println(result);
        }while(!result);




//        while(true) {
//            // Using the below way, the return value is right, I don't know reason.
//            // new a Thread need to spend some time?
//            // Solution: Add the -Djava.compiler=NONE to remove the JVM optimize
//            UnsafeSequence unsafeSequence2 = new UnsafeSequence();
//            CountDownLatch cdl2 = new CountDownLatch(1000);
//            for(int i = 0; i< 1000; i++) {
//                new Thread(() -> {
//                    unsafeSequence2.upCount();
//                    cdl2.countDown();
//
//                }).start();
//            }
//            cdl2.await();
//            System.out.println(unsafeSequence2.getValue());
//            if (unsafeSequence2.getValue() != 1000) {
//                break;
//            }
//        }

        // the wile loop will be broken.
    }
}
