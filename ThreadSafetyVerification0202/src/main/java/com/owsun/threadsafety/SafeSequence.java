package com.owsun.threadsafety;

public class SafeSequence {
    private int count;


    public synchronized void upCount() {
        count++;
    }

    public int getValue() {
        return count;
    }


}
