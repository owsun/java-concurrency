package com.owsun.threadsafety;

import org.openjdk.jmh.annotations.Benchmark;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class VerifyMain {


    private final static int TREAD_COUNT = 10;

    private final static int LOOP_COUNT = 1000;


    public static void main(String[] args) throws InterruptedException, IOException {


        InternetDomainName.from(emailDomain).topDomainUnderRegistrySuffix().toString();




        //ConcurrentHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 1606748500
        //SynchronizedMap use (10) Threads, every Thread will put (1000) item, The Time is : 28310000
        //ThreadSafeHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 28513400
    }

    @Benchmark
    public static void testMap() throws InterruptedException {
        int treadCount = 10;
        int singleThreadLoopCount = 1000;
        testPutPerformance(new ConcurrentHashMap<>(), treadCount, singleThreadLoopCount);
        testPutPerformance(Collections.synchronizedMap(new HashMap<>()), treadCount, singleThreadLoopCount);
        testPutPerformance(new ThreadSafeHashMap(), treadCount, singleThreadLoopCount);
    }



    public static void testPutPerformance(Map<String, String> testMap, int nThreads, int singleThreadLoopCount) throws InterruptedException {
        TestHarness testHarness = new TestHarness();
        long time = testHarness.timeTasks(nThreads, new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i< singleThreadLoopCount; i++) {
                    testMap.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
                }
            }
        });


        System.out.println(testMap.getClass().getSimpleName() + " use (" + nThreads + ") Threads, every Thread will put ("+ singleThreadLoopCount + ") item, The Time is : " + time);
    }








}
