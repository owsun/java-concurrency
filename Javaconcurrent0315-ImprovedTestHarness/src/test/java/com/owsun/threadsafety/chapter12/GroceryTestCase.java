package com.owsun.threadsafety.chapter12;

import com.owsun.threadsafety.chapter11.CopyOnWriteGrocery;
import com.owsun.threadsafety.chapter11.Grocery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class GroceryTestCase extends JSR166TestCase {


    @Test
    public void testMultiThreadAddFruitTestCase() throws InterruptedException {

        Grocery grocery = new CopyOnWriteGrocery();

        SimpleThreadFactory simpleThreadFactory = new SimpleThreadFactory();
        Thread thread1 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addFruit(i, String.valueOf(i));
            }
        });
        Thread thread2 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addFruit(i, String.valueOf(i));
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        threadAssertEquals(20000, grocery.getFruits());
        threadAssertEquals(0, grocery.getVegetables());
    }

    @Test
    public void testMultiThreadAddVegetableTestCase() throws InterruptedException {
        Grocery grocery = new CopyOnWriteGrocery();

        SimpleThreadFactory simpleThreadFactory = new SimpleThreadFactory();
        Thread thread1 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addVegetable(i, String.valueOf(i));
            }
        });
        Thread thread2 = simpleThreadFactory.newThread(() -> {
            for (int i = 0; i < 10000; i++) {
                grocery.addVegetable(i, String.valueOf(i));
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        threadAssertEquals(0, grocery.getFruits());
        threadAssertEquals(20000, grocery.getVegetables());
    }
}
