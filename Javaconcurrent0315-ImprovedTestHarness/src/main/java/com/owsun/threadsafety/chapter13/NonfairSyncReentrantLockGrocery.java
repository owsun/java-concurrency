package com.owsun.threadsafety.chapter13;

import com.owsun.threadsafety.chapter11.Grocery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class NonfairSyncReentrantLockGrocery implements Grocery {


     private final List<String> fruits = new ArrayList<>();
     private final List<String> vegetables = new ArrayList<>();

     private final ReentrantLock fruitReentrantLock = new ReentrantLock(false);
     private final ReentrantLock vegetablesReentrantLock = new ReentrantLock(false);



     public void addFruit(int index, String fruit) {
        boolean ans =   fruitReentrantLock.tryLock();
        if (ans) {
            try{
                fruitReentrantLock.lock();

                fruits.add(index, fruit);
            } finally {
                fruitReentrantLock.unlock();
            }
        }

     }

     public void addVegetable(int index, String vegetable) {

         boolean ans =   vegetablesReentrantLock.tryLock();
         if (ans) {
             try{
                 vegetablesReentrantLock.lock();
                 vegetables.add(index, vegetable);
             } finally {
                 vegetablesReentrantLock.unlock();
             }
         }

     }

    @Override
    public synchronized int getFruits() {
        return fruits.size();
    }

    @Override
    public synchronized int getVegetables() {
        return vegetables.size();
    }
}
