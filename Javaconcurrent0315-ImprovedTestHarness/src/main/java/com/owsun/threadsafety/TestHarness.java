package com.owsun.threadsafety;

public interface TestHarness {

    long timeTasks(int nThreads, final Runnable task) throws Exception;


    long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task) throws Exception;


}
