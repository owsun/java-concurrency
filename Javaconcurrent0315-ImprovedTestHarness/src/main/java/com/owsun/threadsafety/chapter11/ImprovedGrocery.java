package com.owsun.threadsafety.chapter11;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class ImprovedGrocery implements Grocery  {


    private final List<String> fruits = new ArrayList<>();
    private final List<String> vegetables = new ArrayList<>();


    public  void addFruit(int index, String fruit) {

        synchronized(fruits) {
            fruits.add(index, fruit);
        }

    }

    public void addVegetable(int index, String vegetable) {
        synchronized(vegetables) {
            vegetables.add(index, vegetable);
        }
    }

    @Override
    public int getFruits() {
        return fruits.size();
    }

    @Override
    public int getVegetables() {
        return vegetables.size();
    }
}
