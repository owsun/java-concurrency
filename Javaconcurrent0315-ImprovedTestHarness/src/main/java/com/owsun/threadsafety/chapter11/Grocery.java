package com.owsun.threadsafety.chapter11;

public interface Grocery {
    void addFruit(int i, String s);

    void addVegetable(int i, String s);

    int getFruits();

    int getVegetables();
}
