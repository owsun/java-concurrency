package com.owsun.threadsafety.chapter11;


import com.owsun.threadsafety.chapter8.pool.ImprovedTestHarnessPoolImpl;
import com.owsun.threadsafety.chapter8.pool.TestHarnessPool;

import java.util.List;

public class VerifyChapter11Main {

    private static final int AVAILABLE_THREAD_COUNT = Runtime.getRuntime().availableProcessors();
    private static final List<Grocery> TEST_OBJECTS = List.of(
            new SynchronizedGrocery(),
            new ImprovedGrocery(),
            new CopyOnWriteGrocery()
    );

    public static void main(String[] args) throws Exception {
        System.out.println("Threads: " + AVAILABLE_THREAD_COUNT);

        test(new ImprovedTestHarnessPoolImpl());
    }

    public static void test(TestHarnessPool harness) throws Exception {

        System.out.println("Using: " + harness.getClass().getSimpleName());

        System.out.println("Start test...");
        for (Grocery grocery : TEST_OBJECTS) {
            long cost = 0;
            Runnable task = () -> {
                for(int i1 = 0; i1 < 100; i1++) {

                    grocery.addFruit(i1, "Fruit " + System.currentTimeMillis());
                    grocery.addVegetable(i1, "Vegetable " + System.currentTimeMillis());
                }
            };
            cost += harness.timeTasks(AVAILABLE_THREAD_COUNT, 100, task, false);
            System.out.println(grocery.getClass().getSimpleName() + " cost:   " + cost );
        }
    }

}
