package com.owsun.threadsafety;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ImprovedTestHarnessImpl implements TestHarness {

    @Override
    public long timeTasks(int nThreads, final Runnable task) throws Exception {

        final ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        final CyclicBarrier startBarrier = new CyclicBarrier(nThreads + 1);
        final CyclicBarrier endBarrier = new CyclicBarrier(nThreads + 1);

        for (int i = 0; i < nThreads; i++) {
            executorService.submit(() -> {
                try {
                    try {
                        startBarrier.await();
                        task.run();
                    } finally {
                        endBarrier.await();
                    }
                } catch (InterruptedException | BrokenBarrierException ignored) {
                    // ignored
                }
            });
        }

        startBarrier.await();
        long start = System.nanoTime();
        endBarrier.await();
        long end = System.nanoTime();
        executorService.shutdown();
        return end - start;

    }

    @Override
    public long timeTasks(int nThreads, int timeoutInSeconds, Runnable task) throws Exception {
        AtomicLong startTime = new AtomicLong();
        AtomicLong endTime = new AtomicLong();
        final CyclicBarrier startCyclicBarrier = new CyclicBarrier(nThreads + 1, () -> startTime.set(System.nanoTime()));
        final CyclicBarrier endCyclicBarrier = new CyclicBarrier(nThreads + 1,() -> endTime.set(System.nanoTime()));
        final ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        for (int i = 0; i < nThreads; i++) {
            executor.execute(() -> {
                try {
                    startCyclicBarrier.await();
                    ExecutorService threadExecutor = Executors.newSingleThreadExecutor();
                    Future<?> taskExec = threadExecutor.submit(task);
                    try {
                        taskExec.get(timeoutInSeconds, TimeUnit.SECONDS);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        System.out.println(Thread.currentThread().getName() + " was timeout!");
                    } finally {
                        endCyclicBarrier.await();
                        if (!taskExec.isDone()) {
                            taskExec.cancel(true);
                            System.out.println(Thread.currentThread().getName() + " has been canceled.");
                        }
                        threadExecutor.shutdown();
                    }
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            });
        }
        startCyclicBarrier.await();
        endCyclicBarrier.await();
        long totalTime = endTime.get() - startTime.get();
        executor.shutdown();
        return totalTime;
    }


}
