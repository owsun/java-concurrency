package com.owsun.threadsafety;

import java.util.concurrent.CountDownLatch;

public class TestHarnessImpl implements TestHarness{

    @Override
    public long timeTasks(int nThreads, final Runnable task) throws Exception{
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for(int i = 0; i < nThreads; i++) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        startGate.await();
                        try {
                            task.run();
                        }finally {
                            endGate.countDown();
                        }
                    } catch (InterruptedException ignored) {}
                }
            };
            t.start();
        }

        long start = System.nanoTime();
        startGate.countDown();
        endGate.await();
        long end = System.nanoTime();
        return end-start;
    }

    @Override
    public long timeTasks(int nThreads, int timeoutInSeconds, Runnable task) throws Exception {
        return 0;
    }

}
