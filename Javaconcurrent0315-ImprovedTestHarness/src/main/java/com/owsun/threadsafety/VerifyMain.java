package com.owsun.threadsafety;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class VerifyMain {


    private final static int TREAD_COUNT = 10;

    private final static int LOOP_COUNT = 1000;


    public static void main(String[] args) throws Exception {



        Map<Long, List<String>> testMap = new HashMap<>();

        testMap.put(Long.valueOf("10000000"), List.of("1","2"));


        System.out.println(testMap.containsKey(Long.valueOf("10000000")));

        //Map<Long, List<String>> usersOfNotPurgeInspectionAndAttachmentReasons = complianceRegionToPurge.getUsersOfNotPurgeInspectionAndAttachmentReasons();
        testMap.computeIfAbsent(Long.valueOf("10000001"), key -> new ArrayList<>()).addAll( List.of("1","2"));
        testMap.computeIfAbsent(Long.valueOf("10000000"), key -> new ArrayList<>()).addAll( List.of("3","4"));


        System.out.println(testMap);

        //testTestHarnessImpl();

        //System.out.println("========================================================");

       // testImprovedTestHarness();

//        Use the TestHarnessImpl to test ConcurrentHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 1603487600
//        Use the TestHarnessImpl to test SynchronizedMap use (10) Threads, every Thread will put (1000) item, The Time is : 21996800
//        Use the TestHarnessImpl to test ThreadSafeHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 20305800
//                ========================================================
//        Use the ImprovedTestHarness to test ConcurrentHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 15193500
//        Use the ImprovedTestHarness to test SynchronizedMap use (10) Threads, every Thread will put (1000) item, The Time is : 14853200
//        Use the ImprovedTestHarness to test ThreadSafeHashMap use (10) Threads, every Thread will put (1000) item, The Time is : 12843800

        // Verify task will be canceled
//        TestHarness improvedTestHarness = new ImprovedTestHarnessImpl();
//        improvedTestHarness.timeTasks(3,4, ()->{
//            try {
//                TimeUnit.SECONDS.sleep(5);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        });

    }

    public static void testTestHarnessImpl() throws Exception {
        testPutPerformance(new ConcurrentHashMap<>(), TREAD_COUNT, LOOP_COUNT, new TestHarnessImpl());
        testPutPerformance(Collections.synchronizedMap(new HashMap<>()), TREAD_COUNT, LOOP_COUNT, new TestHarnessImpl());
        testPutPerformance(new ThreadSafeHashMap(), TREAD_COUNT, LOOP_COUNT, new TestHarnessImpl());
    }

    public static void testImprovedTestHarness() throws Exception {
        testPutPerformance(new ConcurrentHashMap<>(), TREAD_COUNT, LOOP_COUNT, new ImprovedTestHarnessImpl());
        testPutPerformance(Collections.synchronizedMap(new HashMap<>()), TREAD_COUNT, LOOP_COUNT, new ImprovedTestHarnessImpl());
        testPutPerformance(new ThreadSafeHashMap(), TREAD_COUNT, LOOP_COUNT, new ImprovedTestHarnessImpl());
    }



    public static void testPutPerformance(Map<String, String> testMap, int nThreads, int singleThreadLoopCount, TestHarness testHarness) throws Exception {

        long time = testHarness.timeTasks(nThreads, new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i< singleThreadLoopCount; i++) {
                    testMap.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
                }
            }
        });

        System.out.println("Use the " + testHarness.getClass().getSimpleName() + " to test " + testMap.getClass().getSimpleName() + " use (" + nThreads + ") Threads, every Thread will put ("+ singleThreadLoopCount + ") item, The Time is : " + time);
    }








}
