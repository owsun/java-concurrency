package com.owsun.threadsafety.chapter10;

public class DollarAmount implements Comparable<DollarAmount> {


    private final int amount;

    public DollarAmount(int amount) {
        this.amount = amount;
    }
    @Override
    public int compareTo(DollarAmount o) {
        return this.amount > o.amount ? 1 : 0;
    }
}
