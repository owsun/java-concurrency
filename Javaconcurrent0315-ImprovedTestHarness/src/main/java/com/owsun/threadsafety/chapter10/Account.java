package com.owsun.threadsafety.chapter10;

public class Account {

    public final DollarAmount dollarAmount;


    public Account(DollarAmount dollarAmount) {
        this.dollarAmount = dollarAmount;
    }
    public DollarAmount getBalance() {
        return this.dollarAmount;
    }

    public void debit(DollarAmount amount) {
    }

    public void credit(DollarAmount amount) {
    }
}
