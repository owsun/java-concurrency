package com.owsun.threadsafety.chapter10.homework;

import java.util.concurrent.BrokenBarrierException;

public class VerifyMain {

    public static void main(String[] args) throws Exception {

        LeftRightDeadlock leftRightDeadlock = new LeftRightDeadlock();


        Runnable runnable1 = () -> {
            try {
                leftRightDeadlock.leftRight();
            } catch (BrokenBarrierException e) {
                Thread.currentThread().interrupt();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

        };

        Runnable runnable2 = () -> {
            try {
                leftRightDeadlock.rightLeft();
            } catch (BrokenBarrierException e) {
                Thread.currentThread().interrupt();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        new Thread(runnable1).start();
        new Thread(runnable2).start();






    }










}
