package com.owsun.threadsafety.chapter10.homework;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class LeftRightDeadlock {


    private static final int PARTIES = 2;
    private final CyclicBarrier startBarrier = new CyclicBarrier(PARTIES);

    private final LockObject left = new LockObject();
    private final LockObject right = new LockObject();


    public void leftRight() throws BrokenBarrierException, InterruptedException {

        System.out.println("leftRight: Start");
        synchronized (left) {
            System.out.println("leftRight: Get Left Lok");
            synchronized (right) {
                System.out.println("leftRight: Get Right Lok");

                startBarrier.await();
            }
        }
    }
    public void rightLeft() throws BrokenBarrierException, InterruptedException {

        System.out.println("rightLeft: Start");

        synchronized (right) {
            System.out.println("rightLeft: Get Right Lok");
            synchronized (left) {
                System.out.println("rightLeft: Get Left Lok");
                startBarrier.await();
            }
        }
    }

    public class LockObject{

    }
}
