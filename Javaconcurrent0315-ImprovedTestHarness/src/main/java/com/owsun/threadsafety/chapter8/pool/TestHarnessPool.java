package com.owsun.threadsafety.chapter8.pool;

public interface TestHarnessPool {



    long timeTasks(int nThreads, int timeoutInSeconds, final Runnable task, boolean startAllCoreThreads) throws Exception;


}
