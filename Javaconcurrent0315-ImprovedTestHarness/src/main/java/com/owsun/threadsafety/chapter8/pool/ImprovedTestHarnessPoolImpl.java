package com.owsun.threadsafety.chapter8.pool;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ImprovedTestHarnessPoolImpl implements TestHarnessPool {



    private static final int MAX_POOL_SIZE = 5;

    private static final int KEEP_ALIVE_TIME = 60;
    @Override
    public long timeTasks(int nThreads, int timeoutInSeconds, Runnable task, boolean startAllCoreThreads) throws Exception {
        AtomicLong startTime = new AtomicLong();
        AtomicLong endTime = new AtomicLong();
        final CyclicBarrier startCyclicBarrier = new CyclicBarrier(nThreads + 1, () -> startTime.set(System.nanoTime()));
        final CyclicBarrier endCyclicBarrier = new CyclicBarrier(nThreads + 1,() -> endTime.set(System.nanoTime()));

        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(nThreads + MAX_POOL_SIZE);
        TimingThreadPool executorService = new TimingThreadPool(nThreads, startAllCoreThreads, nThreads + MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, workQueue);
        for (int i = 0; i < nThreads; i++) {
            executorService.execute(() -> {
                try {
                    startCyclicBarrier.await();

                    simulateTimeout(task, timeoutInSeconds, endCyclicBarrier);

                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            });
        }
        startCyclicBarrier.await();
        endCyclicBarrier.await();
        long totalTime = endTime.get() - startTime.get();
        executorService.shutdown();
        return totalTime;
    }

    private void simulateTimeout(final Runnable task, int timeoutInSeconds, final CyclicBarrier endCyclicBarrier) throws BrokenBarrierException, InterruptedException {
        ExecutorService threadExecutor = Executors.newSingleThreadExecutor();
        Future<?> taskExec = threadExecutor.submit(task);
        try {
            taskExec.get(timeoutInSeconds, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println(Thread.currentThread().getName() + " was timeout!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            endCyclicBarrier.await();
            if (!taskExec.isDone()) {
                taskExec.cancel(true);
                System.out.println(Thread.currentThread().getName() + " has been canceled.");
            }
            threadExecutor.shutdown();
        }
    }


}
