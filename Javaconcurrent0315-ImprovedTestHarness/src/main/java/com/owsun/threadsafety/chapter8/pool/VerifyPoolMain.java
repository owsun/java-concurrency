package com.owsun.threadsafety.chapter8.pool;

import java.util.concurrent.TimeUnit;

public class VerifyPoolMain {


    private final static int TREAD_COUNT = 10;

    public static void main(String[] args) throws Exception {


        Runnable task = ()->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
            // Verify task will be canceled
        TestHarnessPool improvedTestHarness = new ImprovedTestHarnessPoolImpl();
        improvedTestHarness.timeTasks(TREAD_COUNT,4, task, true);

        Thread.sleep(3000);
        System.out.println("==============================================================================================");


        TestHarnessPool improvedTestHarness2 = new ImprovedTestHarnessPoolImpl();
        improvedTestHarness2.timeTasks(TREAD_COUNT,4, task, false);

    }
}
