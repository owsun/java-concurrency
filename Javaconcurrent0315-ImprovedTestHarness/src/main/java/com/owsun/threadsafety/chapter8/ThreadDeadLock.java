package com.owsun.threadsafety.chapter8;

import java.util.concurrent.*;

public class ThreadDeadLock {

    ExecutorService executorService = Executors.newSingleThreadExecutor();


    public class ReadPageTask implements Callable<String> {

        @Override
        public String call() throws Exception {

            Future<String> header, footer;
            header = executorService.submit(new LoadFileTask(" Header "));
            footer = executorService.submit(new LoadFileTask(" Footer "));


            String body = readerBody();

            return header.get() + body + footer.get();
        }

        private String readerBody() {

            return " Body ";
        }
    }


    public void readPage() throws ExecutionException, InterruptedException {


        System.out.println("readPage");
        Future<String> page = executorService.submit(new ReadPageTask());

        System.out.println(page.get());
    }
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadDeadLock threadDeadLock = new ThreadDeadLock();

        threadDeadLock.readPage();
    }
}
