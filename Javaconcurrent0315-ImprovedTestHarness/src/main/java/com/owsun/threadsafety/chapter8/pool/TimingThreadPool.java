package com.owsun.threadsafety.chapter8.pool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

public class TimingThreadPool extends ThreadPoolExecutor {

    private static final Logger LOGGER = Logger.getLogger("TimingThreadPool");


    private final ThreadLocal<Long> startTime = new ThreadLocal<>();

    private final AtomicLong numTasks = new AtomicLong();
    private final AtomicLong totalTime = new AtomicLong();

    public TimingThreadPool(int corePoolSize, boolean startAllCoreThreads, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        if (startAllCoreThreads) {
            LOGGER.info("Start all core threads: " + corePoolSize + ", The maximumPoolSize is " + maximumPoolSize);

            //this.prestartAllCoreThreads();
            startAllCoreThreads(corePoolSize);
        }
    }

    private void startAllCoreThreads(int corePoolSize){
        CountDownLatch warnUpTaskCountDown = new CountDownLatch(corePoolSize);
        for(int i = 0; i < corePoolSize; i++){
            this.submit(() -> {
                warnUpTaskCountDown.countDown();
                try {
                    warnUpTaskCountDown.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    if (!Thread.currentThread().isInterrupted()) {
                        Thread.currentThread().interrupt();
                    }
                    throw new RuntimeException(e);
                }
            });
        }
    }

    @Override
    protected void beforeExecute(Thread thread, Runnable runnable) {
        super.beforeExecute(thread, runnable);
        LOGGER.info(String.format("Thread %s: start %s", thread.getName(), runnable.getClass()));
        startTime.set(System.nanoTime());
    }

    @Override
    protected void afterExecute(Runnable runnable, Throwable throwable) {
        try {
            long endTime = System.nanoTime();
            long taskTime = endTime - startTime.get();
            numTasks.incrementAndGet();
            totalTime.addAndGet(taskTime);
            LOGGER.fine(String.format("Thread %s: end  %s, time -> %dns", throwable, runnable, taskTime));
        } finally {
            super.afterExecute(runnable, throwable);
        }
    }

    @Override
    protected void terminated() {
        try {
            LOGGER.info(String.format("Terminated: avg time -> %dns", totalTime.get() / numTasks.get()));
        } finally {
            super.terminated();
        }
    }
}
