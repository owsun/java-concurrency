package com.owsun.threadsafety;

import java.util.HashMap;


public class ThreadSafeHashMap<K,V> extends HashMap<K,V>{


    private final HashMap<K, V> hashMap;

    public ThreadSafeHashMap() {
        hashMap = new HashMap<>();
    }



    @Override
    public V put(K key, V value) {
        synchronized(hashMap) {
            return hashMap.put(key, value);
        }
    }

    @Override
    public V get(Object key) {
        return hashMap.get(key);
    }

    @Override
    public int size() {
        return hashMap.size();
    }
}
