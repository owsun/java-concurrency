package com.owsun.threadsafety;

public class VerifyMain {

    public static void main(String[] args) {
        UnsafeStates unsafeStates = new UnsafeStates();


        String[] states =  unsafeStates.getStates();

        states[0] = "B";

        for (String s : unsafeStates.getStates()) {
            System.out.println(s);
        }

        // The desired result is  A B C
        // The actual result is B B C


        UnsafeFianlStates unsafeFianlStates = new UnsafeFianlStates();


        String[] finalStates =  unsafeFianlStates.getStates();

        finalStates[0] = "B";

        for (String s : unsafeFianlStates.getStates()) {
            System.out.println(s);
        }

        // The desired result is  A B C
        // The actual result is B B C



        SafeStates safeStates = new SafeStates();


        String[] safeStatesArray =  safeStates.getStates();

        safeStatesArray[0] = "B";

        for (String s : safeStates.getStates()) {
            System.out.println(s);
        }

        // The desired result is  A B C
        // The actual result is A B C

    }
}
