package com.owsun.threadsafety;

public class SafeStates {

    private  String[] states = new String[] {
            "A", "B", "C"
    };

    public String[] getStates() {
        String[] copyStates = new String[states.length];
        System.arraycopy(states, 0, copyStates, 0, states.length) ;
        return copyStates;
    }
}
