package com.owsun.threadsafety;

public class UnsafeStates {

    private String[] states = new String[] {
            "A", "B", "C"
    };

    public String[] getStates() {
        return states;
    }
}
